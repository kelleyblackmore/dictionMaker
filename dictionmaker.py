import argparse
from core.core import dictionProcess




def app_start(start, format, path):
    
    if start =='cli':
        dictionProcess(format, path) 
    if start =='web':
        print('web')
    if start =='desktop':
        print('desktop')
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Dictionary Maker')
    
    parser.add_argument(
        '-f',
        '--format',
        choices=['json', 'yml'],
        default='json',
        help='The format dictionary are saved to.')

    parser.add_argument(
         '-p',
        '--path',
        default='sample_files/sample.txt',
        help='The Path the file to be read from ')
    parser.add_argument(
        '-s',
        '--start',
        choices=['web','cli','desktop'],
        default='cli',
        help='Starting the interfaces'

    )
    args = parser.parse_args()
    
    app_start(args.start, args.format, args.path)
    



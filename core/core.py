#! /usr/local/bin/python3.7
import json
from .readHandlers import readFile, copyToJSONTemplate, readJSON, writeJSON
from .model import diction



def blackList_words(words):
    blacklist= ['a','the', 'be', 'no','on','that','he','she','are','in','of','for','and','will']
  
    blackListed =[diction('test')]
    for word in words:
        print(word)
        if word in blacklist:
            print("removining: {}".format(word))
            words.remove(word)
            blacklist_word = diction(word)
            blackListed.append(blacklist_word)
    # testing around
    #for item in blackListed:
    #    item.print_self()
    return words

def print_words_info(words):
    print('PRINTING VALUES AND HOW MANY')
    print(words)
    print(len(words))

def removeSpChars(words):
    finalWords = []
    for word in words:
        word_no_periods  = word.replace('.','')
        word_no_commas = word_no_periods.replace(',','')
        finalWords.append(word_no_commas)
    return finalWords


def simple_parse(text):
    # Find words in text
    words = text.split(" ")
    sentences = text.split(".")
    dictionList = []
    finalWords = []
    fixedWords = blackList_words(words)
    finalWords =  removeSpChars(fixedWords)
    # create objects with the words 
    for word in finalWords:
        newDiction =  diction(word)
        dictionList.append(newDiction)
    #add context to objects
    for sentence in sentences:
        for dictionItem in dictionList:
            if dictionItem.name in sentence:
                dictionItem.add_context(sentence)
    return dictionList
   

def dictionProcess(format, path):
    
    #simple_parse(readFile(path))
    if format == 'json':
        JSON_READ_FILE_PROCESS(path)
def JSON_READ_STING_PROCESS(text):
    dictionList = copyToJSONTemplate(simple_parse(text))
    writeJSON(dictionList,'test.json')

def JSON_READ_FILE_PROCESS(path):
    dictionList =  simple_parse(readFile(path))
    final_json =  copyToJSONTemplate(dictionList)
    writeJSON(final_json,'test.json')

if __name__== "__main__":
    json_template =   'templates/diction.json'
    json_sample = 'sample_files/sample.json'    
    text = "despite being sympathetic to the Guzmans appeal. denied the request on Thursday. however, noting that other defendants who are considered high-security risks wouldn't be granted the favor. For El Chapo, there would be no exception.Guzman has pleaded not guilty charges that he oversaw a Mexican drug cartel, known for violence and for breaking him out of jails.A jury of seven women and five men will hear opening statements next week in a Brooklyn federal courthouse."
   
    # Returns list of dictions

    dictionList =  simple_parse(text)
    final_json =  copyToJSONTemplate(dictionList)
    
    writeJSON(final_json,'test.json')
    

    
    

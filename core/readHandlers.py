import os
import json

template_dictionary ={"Dictionary":[]}
template_diction = {"name":"word","count":0,"context": []}
def readFile(filename):
    txt = open(filename, 'r')
    return txt.read()
    

def copyToJSONTemplate(dictionList):
    print('CONVERTING TO JSON')
    copy_dictionary = template_dictionary.copy()
    for item in dictionList:
        copy_diction = template_diction.copy()
        copy_diction['name'] = item.get_name()
        copy_diction['count'] = item.get_count()
        copy_diction['context'] = item.get_context()
        copy_dictionary["Dictionary"].append(copy_diction)
    print('CONVERASION DONE')
    return copy_dictionary

def writeJSON(data,filePath):
    FINAL_PATH = 'output//{}'.format(filePath)
    with open(FINAL_PATH, 'w') as out:
        json.dump(data, out,indent=2)

    

def readJSON(filePath):
    with open(filePath, 'r') as f:
        json_data = json.loads(f)
    return json_data



    
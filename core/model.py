class diction():
    def __init__(self,name):
        self.name = name
        self.count = 0
        self.context_definitions = []

    def print_self(self):
        print("Word: {}".format(self.name))
        print("Count: {}".format(self.count))
        print("Context Definitions: {}".format(self.context_definitions))
    
    def add_context(self, context):
        self.count +=1
        self.context_definitions.append(context)
    def get_context(self):
        return self.context_definitions
    def add_count(self):
        self.count +=1
    def get_name(self):
        return self.name
    def get_count(self):
        return self.count
   

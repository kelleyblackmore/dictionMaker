#! /usr/local/bin/python3.7
from tkinter import *
from tkinter import ttk



# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):
        
        # parameters that you want to send through the Frame class. 
        Frame.__init__(self, master)   

        #reference to the master widget, which is the tk window                 
        self.master = master

        #with that, we want to then run init_window, which doesn't yet exist
        self.init_window()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget      
        self.master.title("Diction Maker")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)
        
        # create the file object)
        file = Menu(menu)
        save = Menu(menu)
        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)
        save.add_command(label="Save as json", command=self.client_exit)
        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)
        menu.add_cascade(label="Save", menu=save)
        # create the file object)
        edit = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        edit.add_command(label="Undo")

        #added "file" to our menu
        menu.add_cascade(label="Edit", menu=edit)

    
    def client_exit(self):
        exit()

def createTextBox(parent):
    Tbox = Entry(parent)
    Tbox.pack()
def createButton(parent):
    button = ttk.Button(parent, text = 'Click Me')
    button.pack_configure()
    button.pack()
def saveFileButton_event():
    print('Filing file')
def ConvertToJSONButton():
    print('Converting to JSoN')
def ConvertToYMLButton():
    print('Converting to yml')
def createLabel(parent):
    label = ttk.Label(parent, text='Convert to json ')
    label.pack()
# root window created. Here, that would be the only window, but
# you can later have windows within windows.


 
def main():
    root = Tk()
    root.geometry("800x500")
    createButton(root)
    createTextBox(root)
    createLabel(root)
    #creation of an instance
    app = Window(root)

    #mainloop 
    root.mainloop() 
if __name__ == "__main__": main()
   
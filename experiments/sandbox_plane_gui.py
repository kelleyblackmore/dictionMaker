#! /usr/local/bin/python3.7
from tkinter import *
from tkinter import ttk

class Window(Frame):

    def __init__(self, master=None):
       
        Frame.__init__(self, master)              
        self.master = master
        self.init_window()

    def init_window(self):  
        self.master.title("Diction Maker")
        self.pack(fill=BOTH, expand=1)
        menu = Menu(self.master)
        self.master.config(menu=menu)
        file = Menu(menu)
        file.add_command(label="Exit", command=self.client_exit)
        menu.add_cascade(label="File", menu=file)
    #def texas_hello(self):
    #    self.label.config(text = "Howdy, Tkinter") 
     
    def client_exit(self):
        exit()

 
def main():
    root = Tk()
    root.geometry("800x500")
   
    #creation of an instance
    app = Window(root)

    #mainloop 
    root.mainloop() 
if __name__ == "__main__": main()
   
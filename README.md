# Dictionary Maker

## Build Status
[![Build Status](https://travis-ci.org/kelleyblackmore/dictionMaker.svg?branch=master)](https://travis-ci.org/kelleyblackmore/dictionMaker)

[![CircleCI](https://circleci.com/gh/kelleyblackmore/dictionMaker.svg?style=svg)](https://circleci.com/gh/kelleyblackmore/dictionMaker)

## Purpose
 I built this to be able to automate creating dictionaries from books or other forms of text.
 

## Description
This program turns large amount text into a dictionary with pulling context sentences and count of when each word appears.
There is a web ui, desktop ui, cli. 

## Features
| Application | works | Save as JSON | Save as YAML | Save as ipynb | ReadsFiles | ReadStrings | ReadPDF | ReadWebsite |
|-------------|-------|--------------|--------------|---------------|------------|-------------|---------|-------------|
| CLI         | Yes   | Yes          | No           | No            | Yes        | Yes         | No      | No          |
| Desktop     | No    | No           | No           | No            | No         | No          | No      | No          |
| Web         | No    | No           | No           | No            | No         | No          | No      | No          |

## Known Bugs

_Go to Issues tab for current bugs_

## TODO
There is alot more to do.
if you would like to see a new feature write in an issue
https://github.com/kelleyblackmore/dictionMaker/issues

## Usage
- pip install requirements
- dictorMaker cli
- dictorMaker Web UI
- dictorMaker Desktop UI
 
### CLI Usage (WIP)

#### Current CLI

```
python dictionmaker.py -s cli -f json -p sample_files/sample.txt
```
```
usage: dictionmaker.py [-h] [-f {json,yml}] [-p PATH] [-s {web,cli,desktop}]

Dictionary Maker

optional arguments:
  -h, --help            show this help message and exit
  -f {json,yml}, --format {json,yml}
                        The format dictionary are saved to.
  -p PATH, --path PATH  The Path the file to be read from
  -s {web,cli,desktop}, --start {web,cli,desktop}
                        Starting the interfaces
```

#### IDEA for CLI
- dictactor 
	Options
	- readFile
	- readString
	- loadWeb
	- loadDesktop
	Sub Options should be able to take more than one
	- -j : save as json
	- -x : save as xml
	- -y : save as yml 
	- -ip: save as ipybn
	- -e : send to email ( default will be json)
	- -pdf: save as pdf (must be use data format to save as this)



## Requirements
* _pip_
* _Python_ 3.*
* preferred system Linux or Mac

### License

*This project is licensed under the MIT license

### Author 
**_Kris Kelley_**

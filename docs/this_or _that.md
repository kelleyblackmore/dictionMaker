 
 
 ## Data type in json template

 So with this below I was curious what I might prefer.

### this 
 
 "Context Defintions":{
            "context_1" : "the dog said woof, the Cat said meow.",
            "context_2" :"the dog said woof, the Cat said meow."
        }

### Or that 

 
 "Context Defintions":[ 
            "the dog said woof, the Cat said meow.",
            "the dog said woof, the Cat said meow."
            ]
           
        